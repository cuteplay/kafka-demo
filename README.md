# kafka-demo
# 参考文档 http://www.iocoder.cn/Spring-Boot/Kafka/?self

kafka批量发送消息的条件
【数量】batch-size ：超过收集的消息数量的最大条数。
【空间】buffer-memory ：超过收集的消息占用的最大内存。
【时间】linger.ms ：超过收集的时间的最大等待时长，单位：毫秒。


生产者说明
Demo01： 简单的生产消费
Demo02： 批量发送
Demo03： 批量消费

#kafka的消息重试
Kafka 中，消费重试和死信队列，是由 Spring-Kafka 所封装提供的，RabbitMQ自带该功能