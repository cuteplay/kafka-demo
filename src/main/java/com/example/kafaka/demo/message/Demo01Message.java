package com.example.kafaka.demo.message;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * author: cuteG <br>
 * date: 2020/6/23 0:17 <br>
 * description: todo <br>
 */
@Getter
@Setter
@ToString
public class Demo01Message {

    public static final String TOPIC = "DEMO_01";

    /**
     * 编号
     */
    private Integer id;

}