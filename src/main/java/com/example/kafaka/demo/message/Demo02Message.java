package com.example.kafaka.demo.message;

import lombok.Data;

/**
 * author: cuteG <br>
 * date: 2020/6/24 14:35 <br>
 * description: todo <br>
 */
@Data
public class Demo02Message {

    public static final String TOPIC = "DEMO_02";

    private Integer id ;
}