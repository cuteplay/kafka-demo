package com.example.kafaka.demo.message;

import lombok.Data;

/**
 * author: pulei2 <br>
 * date: 2020/6/23 16:38 <br>
 * description: todo <br>
 */
@Data
public class MyDemoMessage {

    public static final String TOPIC = "MyDemo";

    private  String id ;
}
