package com.example.kafaka.demo.consumer;

import com.example.kafaka.demo.message.Demo02Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * author: cuteG <br>
 * date: 2020/6/24 14:52 <br>
 * description: todo <br>
 */
@Component
public class Dem02Consumer {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @KafkaListener(topics = Demo02Message.TOPIC,
            groupId = "demo02-consumer-group-" + Demo02Message.TOPIC)
    public void onMessage(Demo02Message message) {
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
    }

    /**
     * 批量消费消息测试
     * 2020-06-24 15:48:08.624  INFO 19064 --- [ntainer#1-0-C-1] c.e.kafaka.demo.consumer.Dem02Consumer   : [onMessage][线程编号:37,==当前为批量消费测试==消息内容：[Demo02Message(id=1592984858), Demo02Message(id=1592984868), Demo02Message(id=1592984878)]]
     * 2020-06-24 15:48:08.624  INFO 19064 --- [ntainer#1-0-C-1] c.e.kafaka.demo.consumer.Dem02Consumer   : 消费的消息总数为==[3]
     * @param message
     */
    @KafkaListener(topics = Demo02Message.TOPIC,
            groupId = "demo02-consumer-group-" + Demo02Message.TOPIC)
    public void onMessage(List<Demo02Message> message) {
        logger.info("[onMessage][线程编号:{},==当前为批量消费测试==消息内容：{}]", Thread.currentThread().getId(), message);
        logger.info("消费的消息总数为==[{}]",message.size());
    }
}