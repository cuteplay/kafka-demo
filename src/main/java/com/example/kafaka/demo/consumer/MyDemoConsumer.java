package com.example.kafaka.demo.consumer;

import com.example.kafaka.demo.message.MyDemoMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * author: pulei2 <br>
 * date: 2020/6/23 16:40 <br>
 * description: todo <br>
 */
@Component
public class MyDemoConsumer {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @KafkaListener(topics = MyDemoMessage.TOPIC,groupId = "test")
    public void consume(MyDemoMessage msg){
        logger.info("我收到消息了 3q 内容是:[{}]",msg);
    }
}
