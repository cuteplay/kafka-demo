package com.example.kafaka.demo.producer;

import com.example.kafaka.demo.message.MyDemoMessage;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.ExecutionException;

/**
 * author: pulei2 <br>
 * date: 2020/6/23 16:36 <br>
 * description: todo <br>
 */
@Component
public class MyDemoProducer {

    @Resource
    private KafkaTemplate<Object,Object> kafkaTemplate;

    public void syncSend() throws ExecutionException, InterruptedException {

        MyDemoMessage myDemoMessage = new MyDemoMessage();
        myDemoMessage.setId("hahahahahahahhahahahah 傻笑");

        kafkaTemplate.send(MyDemoMessage.TOPIC,myDemoMessage).get();
    }
}
