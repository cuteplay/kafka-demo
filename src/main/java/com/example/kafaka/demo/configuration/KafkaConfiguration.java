//package com.example.kafaka.demo.configuration;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.kafka.listener.DeadLetterPublishingRecoverer;
//import org.springframework.kafka.listener.ErrorHandler;
//import org.springframework.kafka.listener.SeekToCurrentErrorHandler;
//import org.springframework.util.backoff.BackOff;
//import org.springframework.util.backoff.FixedBackOff;
//
///**
// * author: cuteG <br>
// * date: 2020/6/24 16:45 <br>
// * description: todo <br>
// */
//@Configuration
//public class KafkaConfiguration {
//
//    @Bean
//    @Primary
//    public ErrorHandler kafkaErrorHandler(KafkaTemplate<?, ?> template) {
//        // <1> 创建 DeadLetterPublishingRecoverer 对象
//        DeadLetterPublishingRecoverer recoverer = new DeadLetterPublishingRecoverer(template);
//        // <2> 创建 FixedBackOff 对象
//        BackOff backOff = new FixedBackOff(10 * 1000L, 3L);
//        // <3> 创建 SeekToCurrentErrorHandler 对象
//        return new SeekToCurrentErrorHandler(recoverer, backOff);
//    }
//}