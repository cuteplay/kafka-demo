package com.example.kafaka.demo.producer;

import com.example.kafaka.demo.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.support.SendResult;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class MyDemoProducerTest {

    private Logger logger = LoggerFactory.getLogger(getClass());


    @Resource
    private MyDemoProducer producer;

    @Test
    public void syncSend() throws ExecutionException, InterruptedException{
        int id = (int) (System.currentTimeMillis() / 1000);
        producer.syncSend();
        logger.info("发送了哈");

        // 阻塞等待，保证消费
        new CountDownLatch(1).await();
    }

}